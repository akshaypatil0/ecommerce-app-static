module.exports = {
  reactStrictMode: true,
  images: {
    domains: ["fakestoreapi.com"],
  },
  i18n: {
    locales: ["en", "en-in", "hi"],
    defaultLocale: "en-in",
  },
};
