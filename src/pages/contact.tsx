import React, { FC } from "react";
import Head from "../components/head/head";
import L10n from "../components/l10n/l10n";
import classes from "../styles/contact.module.scss";

const ContactUs: FC = () => {
  return (
    <div className={classes.contact}>
      <Head page="Contact us" />
      <h1>
        <L10n literal="contact" />
      </h1>
      <form>
        <div className={classes.formInput}>
          <label htmlFor="name">
            <L10n literal="name" />
          </label>
          <input type="text" id="name" autoFocus />
        </div>
        <div className={classes.formInput}>
          <label htmlFor="email">
            <L10n literal="email" />
          </label>
          <input type="email" id="email" />
        </div>
        <div className={classes.formInput}>
          <label htmlFor="msg">
            <L10n literal="message" />
          </label>
          <textarea id="msg"></textarea>
        </div>
        <button type="button">
          <L10n literal="send" />
        </button>
      </form>
    </div>
  );
};

export default ContactUs;
