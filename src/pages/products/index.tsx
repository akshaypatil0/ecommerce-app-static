import React from "react";
import { useSelector } from "react-redux";
import { RootState } from "../../store/types";
import { fetchProducts } from "../../store/reducers/products";
import { ProductsState } from "../../store/reducers/products/types";
import { ProductGrid } from "../../components";
import { NextPage } from "next";
import { wrapper } from "../../store";
import { ThunkDispatch } from "redux-thunk";
import { ApiCallRequestAction } from "../../store/reducers/api/types";

const Products: NextPage = () => {
  const { list: products } = useSelector<RootState, ProductsState>(
    (state) => state.products
  );

  return (
    <div>
      <ProductGrid products={products} />
    </div>
  );
};

Products.getInitialProps = wrapper.getInitialPageProps((store) => async () => {
  const dispatch = store.dispatch as ThunkDispatch<
    RootState,
    {},
    ApiCallRequestAction
  >;
  const products = store.getState().products.list;
  if (products.length > 5) return;

  await dispatch(fetchProducts({ force: true }));
});
export default Products;
