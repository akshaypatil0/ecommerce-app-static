import { GetStaticPaths, GetStaticProps, NextPage } from "next";
import React from "react";
import { AddToCartButton } from "../../components";
import Image from "next/image";

import classes from "../../styles/product.module.scss";
import Head from "../../components/head/head";
import { Product } from "../../utils/models";
import * as url from "../../utils/urls";
import axios from "axios";

const ProductPage: NextPage<Props> = ({ product }) => {
  if (!product) {
    return null;
  }
  return (
    <div className={classes.product}>
      <Head page={product.title} description={product.description} />
      <div className={classes.img}>
        <Image
          src={product.image}
          alt={product.title}
          layout="fill"
          objectFit="contain"
        />
      </div>
      <div className={classes.info}>
        <h5>{product.title}</h5>
        <p>{product.description}</p>
        <h6>$ {product.price}</h6>
        <div className={classes.delivery}>
          <h6>Delivery Information</h6>
          <li>Get it within 2 days</li>
          <li>Pay on delivery available</li>
          <li>Easy 30 days return & exchange available</li>
        </div>
        <AddToCartButton product={product} />
      </div>
    </div>
  );
};

export const getStaticProps: GetStaticProps<Props> = async ({ params }) => {
  const { id } = params;

  const { data: product } = await axios.get(url.getProductById(+id as number));
  return {
    props: {
      product,
    },
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const { data: products } = await axios.get<Product[]>(url.getProducts());

  return {
    paths: products.map((product) => ({
      params: { id: product.id.toString() },
    })),
    fallback: true,
  };
};

interface Props {
  product: Product;
}
export default ProductPage;
