import React from "react";
import { GetStaticPaths, GetStaticProps, NextPage } from "next";
import { ProductGrid } from "../../components";
import { Category, Product } from "../../utils/models";
import axios from "axios";

import * as url from "../../utils/urls";

const CategoryPage: NextPage<Props> = ({ products }) => {
  return (
    <div>
      <ProductGrid products={products} />
    </div>
  );
};

export const getStaticProps: GetStaticProps<Props> = async ({ params }) => {
  const { category } = params;

  const { data: products } = await axios.get(
    url.getProducstByCategory(category as string)
  );

  return {
    props: {
      products,
    },
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const { data: categories } = await axios.get<Category[]>(
    url.getAllCategories()
  );

  return {
    paths: categories.map((category) => ({ params: { category } })),
    fallback: true,
  };
};

interface Props {
  products: Product[];
}

export default CategoryPage;
