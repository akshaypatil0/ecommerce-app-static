import axios from "axios";
import { GetStaticProps, NextPage } from "next";
import React from "react";
import { Carousel, ProductGrid, ProductTile, SeeMore } from "../components";
import Head from "../components/head/head";
import L10n from "../components/l10n/l10n";
import classes from "../styles/home.module.scss";
import { Category, Product } from "../utils/models";

import * as url from "../utils/urls";

const Home: NextPage<Props> = ({
  products,
  categories,
  productsByCategory,
}) => {
  return (
    <div className={classes.home}>
      <Head page="Home" />
      <section>
        <div className={classes.heading}>
          <h1>
            <L10n literal="featured_products" />
          </h1>
        </div>
        <ProductGrid
          products={products}
          limit={5}
          renderSeeMore={() => <SeeMore link="/products" />}
        />
      </section>
      {categories.map((category) => (
        <section key={category}>
          <div className={classes.heading}>
            <h1>
              <L10n literal="explore" cue={category} />
            </h1>
          </div>
          {productsByCategory[category] && (
            <Carousel
              items={productsByCategory[category].map((product) => (
                <ProductTile product={product} key={product.id} />
              ))}
              renderSeeMore={() => (
                <SeeMore title={category} link={`/category/${category}`} />
              )}
            />
          )}
        </section>
      ))}
    </div>
  );
};

export default Home;

export const getStaticProps: GetStaticProps<Props> = async () => {
  const { data: products } = await axios.get(url.getProducts(5));
  const { data: categories } = await axios.get<Category[]>(
    url.getAllCategories()
  );

  const categoriesData = await Promise.all(
    categories.map(async (category) => {
      const { data: products } = await axios.get(
        url.getProducstByCategory(category, 4)
      );
      return { [category]: products };
    })
  );

  const productsByCategory = categoriesData.reduce((obj, categoryData) => {
    return { ...obj, ...categoryData };
  }, {});

  return {
    props: {
      products,
      categories,
      productsByCategory,
    },
  };
};

interface Props {
  products: Product[];
  categories: Category[];
  productsByCategory: {
    [category: string]: Product[];
  };
}
