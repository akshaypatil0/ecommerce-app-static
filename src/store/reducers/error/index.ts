import { ActionCreator, Reducer } from "redux";
import { ErrorActions, ErrorState, SetErrorAction } from "./types";

// Action creators
export const setError: ActionCreator<SetErrorAction> = (error: string) => ({
  type: "set-error",
  payload: error,
});

//reducer
const initialState: ErrorState = null;

const errorReducer: Reducer<ErrorState, ErrorActions> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case "set-error":
      return action.payload;

    default:
      return state;
  }
};

export default errorReducer;
