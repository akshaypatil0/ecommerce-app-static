import { ActionCreator, Reducer } from "redux";
import { ThunkAction } from "redux-thunk";
import * as url from "../../../utils/urls";
import { RootState } from "../../types";
import { ApiCallRequestAction } from "../api/types";
import { CategoriesActions, CategoriesState } from "./types";

// Action creators

export const fetchAllCategories: ActionCreator<
  ThunkAction<
    Promise<void>,
    RootState,
    {},
    ApiCallRequestAction<CategoriesActions>
  >
> = ({ limit, force }: { limit?: number; force?: boolean }) => {
  return async (dispatch, getState): Promise<void> => {
    const { lastFetched } = getState().categories;

    await dispatch({
      type: "api-call-request",
      payload: {
        url: url.getAllCategories(),
        onSuccess: "fetch-all-categories",
      },
      meta: force
        ? {
            limit,
          }
        : {
            limit,
            lastFetched,
            cacheDuration: 10,
          },
    });
  };
};

export const fetchProductsByCategory: ActionCreator<
  ThunkAction<
    Promise<void>,
    RootState,
    {},
    ApiCallRequestAction<CategoriesActions>
  >
> = ({ category, limit, force }) => {
  return async (dispatch, getState): Promise<void> => {
    const currentState = getState().categories.productsByCategory?.[category];

    await dispatch({
      type: "api-call-request",
      payload: {
        url: url.getProducstByCategory(category, limit),
        onRequest: "fetch-category-request",
        onSuccess: "fetch-category",
        onFailure: "fetch-category-failed",
      },
      meta: force
        ? {
            category,
          }
        : {
            category,
            lastFetched: currentState?.lastFetched,
            cacheDuration: 10,
          },
    });
  };
};

// Reducer

const initialState: CategoriesState = {
  list: [],
  productsByCategory: {},
};

const categoriesReducer: Reducer<CategoriesState, CategoriesActions> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case "fetch-all-categories":
      return {
        ...state,
        list: action.payload,
        lastFetched: Date.now(),
      };

    case "fetch-category-request":
      return {
        ...state,
        productsByCategory: {
          ...state.productsByCategory,
          [action.meta.category]: {
            ...state.productsByCategory[action.meta.category],
            isFetching: true,
            lastFetched: Date.now(),
          },
        },
      };
    case "fetch-category":
      return {
        ...state,
        productsByCategory: {
          ...state.productsByCategory,
          [action.meta.category]: {
            ...state.productsByCategory[action.meta.category],
            list: action.payload,
            isFetching: false,
          },
        },
      };
    case "fetch-category-failed":
      return {
        ...state,
        productsByCategory: {
          ...state.productsByCategory,
          [action.meta.category]: {
            ...state.productsByCategory[action.meta.category],
            isFetching: false,
          },
        },
      };

    default:
      return state;
  }
};

export default categoriesReducer;
