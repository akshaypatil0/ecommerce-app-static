import { ActionCreator, Reducer } from "redux";
import { CartItem, Product } from "../../../utils/models";
import {
  AddToCartAction,
  CartActions,
  CartState,
  ChangeQuantityAction,
  GetLocalCartAction,
  RemoveFromCartAction,
} from "./types";

// Action creators
export const getLocalCart: ActionCreator<GetLocalCartAction> = () => ({
  type: "get-local-cart",
});

export const addToCart: ActionCreator<AddToCartAction> = (
  product: Product
) => ({
  type: "add-to-cart",
  payload: { product },
});

export const removeFromCart: ActionCreator<RemoveFromCartAction> = (
  productId: Product["id"]
) => ({
  type: "remove-from-cart",
  payload: { productId },
});

export const changeQuantity: ActionCreator<ChangeQuantityAction> = (
  productId: Product["id"],
  newQuantity: CartItem["quantity"]
) => ({
  type: "change-quantity",
  payload: { productId, newQuantity },
});

// reducer

const initialState = getLocalCartState();

const cartReducer: Reducer<CartState, CartActions> = (
  state = initialState,
  action
) => {
  let newState = state;
  switch (action.type) {
    case "get-local-cart":
      newState = getLocalCartState();
      break;

    case "add-to-cart":
      newState = [...state, { product: action.payload.product, quantity: 1 }];
      break;

    case "remove-from-cart":
      newState = state.filter(
        (item) => item.product.id !== action.payload.productId
      );
      break;

    case "change-quantity":
      newState = state.map((item) =>
        item.product.id === action.payload.productId
          ? { ...item, quantity: action.payload.newQuantity }
          : item
      );
      break;

    default:
      return state;
  }

  setLocalCartState(newState);
  return newState;
};

export default cartReducer;

function setLocalCartState(cart: CartState) {
  try {
    localStorage.setItem("cart", JSON.stringify(cart));
  } catch (err) {}
}

function getLocalCartState(): CartState {
  try {
    const localCart = localStorage.getItem("cart");
    return localCart ? JSON.parse(localCart) : [];
  } catch (err) {
    return [];
  }
}
