import { apiCallFailed, apiCallRequest } from "../reducers/api";
import axios from "axios";
import { Middleware } from "redux";
import { Actions, RootState } from "../types";

const api: Middleware<{}, RootState> =
  ({ dispatch }) =>
  (next) =>
  async (action: Actions) => {
    if (action.type !== "api-call-request") return next(action);

    const { url, method, data, onRequest, onSuccess, onFailure } =
      action.payload;
    const { lastFetched, cacheDuration } = action.meta;

    if (lastFetched) {
      if (timeDiffInMin(lastFetched) < (cacheDuration || 10)) return;
    }
    dispatch(apiCallRequest());
    if (onRequest) {
      dispatch({ type: onRequest, meta: action.meta });
    }
    try {
      const res = await axios.request({ url, method, data });
      return dispatch({
        type: onSuccess,
        payload: res.data,
        meta: action.meta,
      });
    } catch (err) {
      if (onFailure) {
        return dispatch({
          type: onFailure,
          payload: err,
        });
      }
      return dispatch(apiCallFailed(err));
    }
  };

const timeDiffInMin = (t: number) => {
  return Math.floor((Date.now() - t) / (1000 * 60));
};

export default api;
