import React, { FC } from "react";
import classes from "./spinner.module.scss";

const LoadingSpinner: FC = () => {
  return (
    <div className={classes.spinner}>
      <div className={classes.dot}></div>
      <div className={classes.dot}></div>
      <div className={classes.dot}></div>
    </div>
  );
};

export default LoadingSpinner;
