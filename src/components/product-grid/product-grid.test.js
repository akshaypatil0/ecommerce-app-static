import React from "react";
import { shallow } from "enzyme";
import { Product } from "../../utils/models";
import ProductGrid, { ProductGridItem } from "./product-grid";
import { LoadingSpinner, ProductTile, SeeMore } from "..";

describe("Renders products list properly", () => {
  it("Handles empty list", () => {
    let products = [];
    let wrapper = shallow(<ProductGrid products={products} />);
    expect(wrapper.find("h6").text()).toContain("Nothing to display !");
  });
  it("Handles products list", () => {
    let products = data.products;
    let wrapper = shallow(<ProductGrid products={products} />);
    expect(wrapper.find(ProductTile).length).toBe(products.length);
  });
});

describe("Renders loading state properly", () => {
  let products = getProducts(3);

  it("Shows spinner when loading", () => {
    let wrapper = shallow(<ProductGrid products={products} isLoading={true} />);
    expect(wrapper.contains(<LoadingSpinner />)).toBe(true);
  });
  it("Does not spinner when not loading", () => {
    let wrapper = shallow(
      <ProductGrid products={products} isLoading={false} />
    );
    expect(wrapper.contains(<LoadingSpinner />)).toBe(false);
  });
});

describe("Renders see more properly", () => {
  it("Renders see more", () => {
    let products = data.products;
    let seeMoreElement = <SeeMore link="sample" />;
    let wrapper = shallow(
      <ProductGrid products={products} renderSeeMore={() => seeMoreElement} />
    );
    expect(wrapper.find(ProductGridItem).length).toBe(products.length + 1);
    expect(
      wrapper
        .childAt(products.length)
        .equals(<ProductGridItem>{seeMoreElement}</ProductGridItem>)
    ).toEqual(true);
  });
});

describe("Renders limit properly", () => {
  it("Shows specified no of products", () => {
    let products = data.products;
    let limit = 3;
    let wrapper = shallow(<ProductGrid products={products} limit={limit} />);
    expect(wrapper.find(ProductGridItem).length).toBe(limit);
  });
  it("Handles invalid limit", () => {
    let products = data.products;
    let wrapper = shallow(
      <ProductGrid products={products} limit={products.length + 2} />
    );
    expect(wrapper.find(ProductGridItem).length).toBe(products.length);
  });
});

var data = {
  products: [1, 2, 3, 4].map((v) => ({
    id: v,
    title: `product ${v}`,
    price: v * 10,
    category: "category",
    description: `sample product ${v}`,
    image: `image${v}`,
  })),
};
