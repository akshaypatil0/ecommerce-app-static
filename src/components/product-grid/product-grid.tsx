import React, { FC, ReactElement } from "react";
import { Product } from "../../utils/models";
import { LoadingSpinner } from "..";
import ProductTile from "../product-tile";
import classes from "./product-grid.module.scss";

const ProductGrid: FC<Props> = ({
  products,
  isLoading,
  renderSeeMore,
  limit,
}) => {
  if (isLoading) {
    return <LoadingSpinner />;
  }
  if (!products || products.length < 1) {
    return (
      <div>
        <h6>Nothing to display !</h6>
      </div>
    );
  }
  return (
    <div className={classes.productGrid}>
      {products.slice(0, limit).map((product) => (
        <ProductTile product={product} key={product.id} />
      ))}
      {renderSeeMore && renderSeeMore()}
    </div>
  );
};

interface Props {
  products: Product[];
  isLoading?: boolean;
  renderSeeMore?: () => ReactElement;
  limit?: number;
}

export default ProductGrid;
