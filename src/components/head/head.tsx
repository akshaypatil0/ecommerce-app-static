import HtmlHead from "next/head";
import { FC } from "react";

const Head: FC<Props> = ({ page, description }) => {
  return (
    <HtmlHead>
      <title>{page} | Ecommerce App</title>
      <meta
        name="description"
        content={description || "Simple ecommerce app"}
      />
    </HtmlHead>
  );
};

interface Props {
  page: string;
  description?: string;
}

export default Head;
