import React, { useEffect, useState, FC } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../store/types";
import { Product } from "../../utils/models";
import { addToCart } from "../../store/reducers/cart";
import { CartState } from "../../store/reducers/cart/types";
import AddToCartButton from "./add-to-cart";

const AddToCartButtonContainer: FC<Props> = ({ product }) => {
  const cart: any = useSelector<RootState, CartState>((state) => state.cart);
  const dispatch = useDispatch();

  const [isInCart, setIsInCart] = useState(false);

  useEffect(() => {
    setIsInCart(cart.find((item: any) => item.product.id === product.id));
  }, [cart, product.id]);

  function handleAdd() {
    dispatch(addToCart(product));
    setIsInCart(true);
  }

  return <AddToCartButton handleAdd={handleAdd} isAdded={isInCart} />;
};

interface Props {
  product: Product;
}

export default AddToCartButtonContainer;
