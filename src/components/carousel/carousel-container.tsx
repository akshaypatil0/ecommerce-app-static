import React, {
  useState,
  FC,
  ReactElement,
  useMemo,
  useEffect,
  useRef,
  UIEventHandler,
} from "react";
import Carousel from "./carousel";
import CarouselTile from "./carousel-tile";

const CarouselContainer: FC<Props> = ({ items, renderSeeMore }) => {
  const [selectedIndex, setSelectedIndex] = useState(0);

  const carouselRef = useRef<HTMLDivElement>(null);

  const scrollToTile = (i: number) => {
    const node = carouselRef.current;
    if (!node) return;

    const selectedTile = node.childNodes[i] as HTMLDivElement;
    if (!selectedTile) return;
    node.scrollLeft =
      selectedTile.offsetLeft -
      node.offsetWidth / 2 +
      selectedTile.offsetWidth / 2;
  };

  const waiting = useRef(false);
  const handleScroll: UIEventHandler<HTMLDivElement> = (e) => {
    const node = carouselRef.current;
    if (!node) return;

    if (waiting.current) {
      return;
    }
    waiting.current = true;

    const firstTile = node.childNodes[0] as HTMLDivElement;
    const firstTileCenter = firstTile.offsetLeft + firstTile.offsetWidth / 2;
    const relativeScroll =
      node.scrollLeft + node.offsetWidth / 2 - firstTileCenter;
    const i = Math.round(relativeScroll / firstTile.offsetWidth);
    if (i >= 0 && i !== selectedIndex) setSelectedIndex(i);

    setTimeout(() => {
      waiting.current = false;
    }, 100);
  };

  var noOfTiles = useMemo(() => {
    return renderSeeMore ? items?.length : items?.length - 1;
  }, [items, renderSeeMore]);

  function next() {
    const ni = selectedIndex + 1 < noOfTiles ? selectedIndex + 1 : noOfTiles;
    scrollToTile(ni);
  }
  function previous() {
    const ni = selectedIndex - 1 < noOfTiles ? selectedIndex - 1 : noOfTiles;
    scrollToTile(ni);
  }

  const tiles = useMemo(() => {
    return (
      <>
        {items.map((tile, i) => (
          <CarouselTile selected={i === selectedIndex} key={i}>
            {tile}
          </CarouselTile>
        ))}
        {renderSeeMore && (
          <CarouselTile selected={selectedIndex === noOfTiles}>
            {renderSeeMore()}
          </CarouselTile>
        )}
      </>
    );
  }, [items, selectedIndex, noOfTiles, renderSeeMore]);

  useEffect(() => {
    scrollToTile(0);
  }, []);

  return (
    <Carousel
      tiles={tiles}
      carouselRef={carouselRef}
      handleScroll={handleScroll}
      // handleScroll={() => {}}
      previous={previous}
      next={next}
    />
  );
};

interface Props {
  items: JSX.Element[];
  renderSeeMore?: () => ReactElement;
}

export default CarouselContainer;
