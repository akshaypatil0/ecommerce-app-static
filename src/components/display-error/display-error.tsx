import React, { FC } from "react";
import classes from "./error.module.scss";

const DisplayError: FC<Props> = ({ error, handleClose }) => {
  if (error) {
    return (
      <div className={classes.error}>
        <p>{error}</p>
        <button onClick={handleClose}>x</button>
      </div>
    );
  }

  return <div />;
};

interface Props {
  error: string | null;
  handleClose: () => void;
}

export default DisplayError;
