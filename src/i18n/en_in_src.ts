import en_src from "./en_src";
import hi_src from "./hi_src";
import { LiteralSrc } from "./literals.types";

const en_in_src: LiteralSrc = {
  ...en_src,
  convertCurrency: hi_src.convertCurrency,
};

export default en_in_src;
