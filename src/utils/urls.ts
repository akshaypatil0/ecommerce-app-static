// const API_URL = "https://fakestoreapi.com";
const API_URL = "http://localhost:3000/api";

export const getProducts = (limit?: number) =>
  `${API_URL}/products${setLimit(limit)}`;

export const getProductById = (id: number) => `${API_URL}/products/${id}`;

export const getAllCategories = () => `${API_URL}/products/categories`;
export const getProducstByCategory = (category: string, limit?: number) =>
  `${API_URL}/products/category/${category}${setLimit(limit)}`;

const setLimit = (limit?: number) => (limit ? `?limit=${limit}` : "");
